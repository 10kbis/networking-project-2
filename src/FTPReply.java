import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FTPReply {
    String msg;

    public FTPReply(int code, String msg) {
        this.msg = code + " " + msg;
    }

    public FTPReply(String msg) {
        this.msg = msg;
    }

    public static FTPReply START_TRANSFER               = new FTPReply(125, "Data connection open, starting transfer");
    public static FTPReply CMD_OK                       = new FTPReply(200, "Command Okay.");
    public static FTPReply SERVICE_READY                = new FTPReply(210, "Service ready for new user.");
    public static FTPReply CLOSE_DATA_CONNECTION        = new FTPReply(226, "Requested action taken. Closing data connection");
    public static FTPReply LOGGED_IN                    = new FTPReply(230, "User logged in, proceed.");
    public static FTPReply ACTION_TAKEN                 = new FTPReply(250, "Requested action taken.");
    public static FTPReply NEED_PASSWORD                = new FTPReply(331, "User name okay, need password.");
    public static FTPReply MORE_INFO_NEEDED             = new FTPReply(350, "Requested file action pending further information.");
    public static FTPReply CANT_OPEN_DATA_CONNECTION    = new FTPReply(425, "Can't open data connection.");
    public static FTPReply TRANSFER_ABORTED             = new FTPReply(426, "Connection closed, transfer aborted.");
    public static FTPReply INTERNAL_ERROR               = new FTPReply(451, "Internal error.");
    public static FTPReply UNKNOWN_CMD                  = new FTPReply(500, "Syntax error, command unrecognized.");
    public static FTPReply ERROR_PARAM                  = new FTPReply(501, "Syntax error in parameters or arguments.");
    public static FTPReply BAD_SEQUENCE                 = new FTPReply(503, "Bad sequence of commands.");
    public static FTPReply NOT_IMPL_PARAM               = new FTPReply(504, "Command not implemented for that parameter.");
    public static FTPReply NOT_LOGGED_IN                = new FTPReply(530, "Not logged in.");
    public static FTPReply NOT_FOUND                    = new FTPReply(550, "Requested action not taken. File or directory not found.");
    public static FTPReply NOT_ENOUGH_PRIVILEGES        = new FTPReply(550, "Requested action not taken. Not enough privileges.");
    public static FTPReply FILENAME_NOT_ALLOWED         = new FTPReply(553, "Requested action not taken. Filename not allowed.");

    @Override
    public String toString() {
        return msg;
    }

    public static FTPReply makePathReply(Path path) {
        String msg = "\"" + path + "\" created.";
        return new FTPReply(257, msg);
    }

    public static FTPReply makePassiveReply(InetAddress address, int port) {
        int high_port = port / 256;
        int low_port = port % 256;
        byte[] components = address.getAddress();
        String msg = "Entering passive mode (" +
                components[0] + "," +
                components[1] + "," +
                components[2] + "," +
                components[3] + "," +
                high_port + "," +
                low_port + ").";
        return new FTPReply(229, msg);
    }

    public static FTPReply makeSystemNameReply(String name) {
        return new FTPReply(215, name);
    }

    public static FTPReply makeFeatureReply(List<String> features) {
        // HOLY FUCK FOR REAL FTP ???
        // Every single command is "CODE SPACE MESSAGE" except this one where a '-'
        // is require instead of the space ONLY if features are supported
        StringBuilder builder = new StringBuilder();
        if (features.size() == 0) {
            builder.append("211 No feature"); // Must start with "211 "
        } else {
            // Must start with a "211-" for some unknown reason...
            builder.append("211-Extensions supported:\r\n");
            for (String feature: features) {
                builder.append(" ").append(feature).append("\r\n");
            }
            builder.append("211 END\r\n");
        }
        return new FTPReply(builder.toString());
    }

    public static FTPReply makeModificationTimeReply(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
        return new FTPReply(213, format.format(date));
    }
}
