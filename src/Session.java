import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.Socket;

/**
 * A POJO session for ProtocolInterpreter.
 *
 * Note: No getter/setter as this class is just a POJO
 */
public class Session {
    public Socket host;
    public BufferedReader in_stream;
    public BufferedWriter out_stream;

    public User user; // null means anonymous if connected
    public boolean logged_in = false;
    public String try_connect_as_user;
    public Path working_dir = new Path();
    public FTPType type = FTPType.ASCII;
    public DataTransferProcess transferProcess;
    public String previous_cmd;
    public Path rename_from;
}
