import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class DataTransferProcess {
    private static final Logger logger = Logger.getLogger(DataTransferProcess.class.getName());

    private static int timeout = 10 * 60 * 1000; // 10 minutes
    private boolean active;
    private Socket data_socket;
    private InputStream in_stream;
    private OutputStream out_stream;
    private InetSocketAddress address;
    private final FTPType type;
    private int port;
    private ServerSocket server_data_socket;

    private DataTransferProcess(boolean active, InetSocketAddress address, int port, FTPType type) {
        this.active = active;
        this.address = address;
        this.port = port;
        this.type = type;
    }

    public static DataTransferProcess newActiveConnection(InetSocketAddress address, FTPType type) {
        return new DataTransferProcess(
                true,
                address,
                0,
                type
        );
    }

    public static DataTransferProcess newPassiveConnection(int port, FTPType type) throws DataConnectionNotOpened {
        DataTransferProcess dtp = new DataTransferProcess(
                false,
                null,
                port,
                type
        );

        dtp.setupPassive();
        return dtp;
    }

    private void setupPassive() throws DataConnectionNotOpened {
        try {
            server_data_socket = new ServerSocket(port);
            server_data_socket.setSoTimeout(timeout);
        } catch (IOException e) {
            throw new DataConnectionNotOpened();
        }
    }

    public void connect() throws DataConnectionNotOpened {
        try {
            if (active) {
                data_socket = new Socket();
                data_socket.connect(address, timeout);
            } else {
                data_socket = server_data_socket.accept();
                server_data_socket.close();
            }

            in_stream = data_socket.getInputStream();
            out_stream = data_socket.getOutputStream();
        } catch (IOException ex) {
            throw new DataConnectionNotOpened();
        }
    }

    public void downloadFromHost(File file) throws TransferAborted, DataConnectionNotOpened {
        if (!isConnected()) {
            logger.info("Could not download from host, no data connection");
            throw new DataConnectionNotOpened();
        }

        try {
            byte[] data = in_stream.readAllBytes();
            if (type == FTPType.BINARY) {
                file.setContent(data);
            } else {
                String data_str = new String(data, StandardCharsets.US_ASCII);
                data_str = data_str.replaceAll("\\r\\n?", "\n").replace("\n", "\r\n");
                file.setContent(data_str.getBytes(StandardCharsets.US_ASCII));
            }

            disconnect();
        } catch (IOException e) {
            throw new TransferAborted();
        }
    }

    public void uploadToHost(File file) throws TransferAborted, DataConnectionNotOpened {
        if (!isConnected()) {
            logger.info("Could not upload to host, no data connection");
            throw new DataConnectionNotOpened();
        }

        try {
            if (type == FTPType.BINARY) {
                out_stream.write(file.getContent());
            } else {
                String data = new String(file.getContent(), StandardCharsets.US_ASCII);
                data = data.replaceAll("\\r\\n?", "\n").replace("\n", "\r\n");

                out_stream.write(data.getBytes(StandardCharsets.US_ASCII));
            }
            out_stream.flush();
            disconnect();
        } catch (IOException e) {
            throw new TransferAborted();
        }
    }

    public void sendDirectoryListing(List<FSNode> nodes) throws DataConnectionNotOpened, TransferAborted {
        String node_delimiter = "\r\n";
        String field_delimiter = "  ";
        SimpleDateFormat format = new SimpleDateFormat("MM dd HH:mm");
        String now = format.format(new Date());

        if (!isConnected()) {
            logger.info("Could not send directory listing, no data connection");
            throw new DataConnectionNotOpened();
        }

        StringBuilder listing = new StringBuilder();
        for (FSNode node : nodes) {
            String permissions;
            int size;

            if (node instanceof Directory) {
                Directory dir = (Directory) node;
                permissions = "d";
                size = dir.size();
            } else {
                File file = (File) node;
                permissions = "-";
                size = file.getContent().length;
            }
            permissions += node.getOwner() == null ? "rwxrwxrwx": "rwxrwx---";
            String owner = node.getOwner() == null ? "nobody": node.getOwner().getName();

            listing.append(permissions).append(field_delimiter)
                .append(0).append(field_delimiter)
                .append(owner).append(field_delimiter) // user owner
                .append(owner).append(field_delimiter) // group owner
                .append(size).append(field_delimiter)
                .append(now).append(field_delimiter)
                .append(node.getName()).append(field_delimiter)
                .append(node_delimiter);
        }

        try {
            String result = listing.toString();
            logger.info(result);
            out_stream.write(result.getBytes(StandardCharsets.US_ASCII));
            disconnect();
        } catch (IOException e) {
            throw new TransferAborted();
        }
    }

    private void disconnect() throws IOException {
        data_socket.close();
    }

    public boolean isConnected() {
        return data_socket != null && !data_socket.isClosed();
    }
}
