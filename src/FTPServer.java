import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FTPServer {
    public static void main(String[] args) {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT][%4$s][%3$s] %5$s%6$s%n");
        Logger logger = Logger.getLogger(FTPServer.class.getName());

        int max_thread;
        try {
            max_thread = Integer.parseInt(args[0]);
        } catch (Throwable ex) {
            logger.severe("max_thread argument not valid");
            return;
        }


        FileSystem fs = new FileSystem();
        ProtocolInterpreter pi = new ProtocolInterpreter(2147, 2047, max_thread, fs);

        User sam = new User("Sam", "123456");
        pi.addUser(sam);

        try {
            fs.makeDirectory("/private", sam);

            File secret = fs.makeFile("/private/secret.txt", sam);
            String secret_content = "UPUPDOWNDOWNLEFTRIGHTLEFTRIGHTBASTART";
            secret.setContent(secret_content.getBytes(StandardCharsets.US_ASCII));

            File mytext = fs.makeFile("/mytext.txt", null);
            String mytext_content = "Irasshaimase";
            mytext.setContent(mytext_content.getBytes(StandardCharsets.US_ASCII));

            byte[] myImg = {66,  77,  70,  1,  0,  0,    0,   0,   0,   0,  62,   0,   0,  0,   40,   0,
                    0,   0,  34,  0,  0,  0,   33,   0,   0,   0,   1,   0,   1,  0,    0,   0,
                    0,   0,   8,  1,  0,  0,    0,   0,   0,   0,   0,   0,   0,  0,    0,   0,
                    0,   0,   0,  0,  0,  0,    0,   0,   0,   0,  -1,  -1,  -1,  0,   -1,  -1,
                    -1,  -1, -64,  0,  0,  0,   -1, -32,   0,   1, -64,   0,   0,  0, -128,  31,
                    -1,  -2, -64,  0,  0,  0,  -65, -33,  -1,  -2, -64,   0,   0,  0,  -65, -33,
                    -1,  -2, -64,  0,  0,  0,  -65, -33,  -1,  -8, -64,   0,   0,  0,  -65, -33,
                    -1,  -2, -64,  0,  0,  0,  -65, -33,  -1,  -2, -64,   0,   0,  0,  -65, -33,
                    -1,  -2, -64,  0,  0,  0,  -65, -33,  -1,  -2, -64,   0,   0,  0,  -65, -33,
                    -1,  -2, -64,  0,  0,  0,  -65, -33,  -1,  -8, -64,   0,   0,  0,  -65, -33,
                    -1,  -2, -64,  0,  0,  0,  -65, -33,  -1,  -2, -64,   0,   0,  0,  -65, -33,
                    -1,  -2, -64,  0,  0,  0,  -65, -33,  -1,  -2, -64,   0,   0,  0,  -65, -33,
                    -1,  -2, -64,  0,  0,  0, -128,  31,  -1,  -8, -64,   0,   0,  0,   -1, -17,
                    -1,  -2, -64,  0,  0,  0,   -1,  -9,  -1,  -2, -64,   0,   0,  0,   -1,  -5,
                    -1,  -2, -64,  0,  0,  0,   -1,  -3,  -2,   0, -64,   0,   0,  0,   -1,  -2,
                    -3,  -1, -64,  0,  0,  0,   -1,  -1, 126,  -1, -64,   0,   0,  0,   -1,  -1,
                    127, 127, -64,  0,  0,  0,   -1,  -1, -65, 127, -64,   0,   0,  0,   -1,  -1,
                    -33, -65, -64,  0,  0,  0,   -1,  -1, -17, -65, -64,   0,   0,  0,   -1,  -1,
                    -17, -33, -64,  0,  0,  0,   -1,  -1,  -9, -33, -64,   0,   0,  0,   -1,  -1,
                    -9, -33, -64,  0,  0,  0,   -1,  -1,  -8,  31, -64,   0,   0,  0,   -1,  -1,
                    -1,  -1, -64,  0,  0,  0
            };
            File image = fs.makeFile("myimage.bmp", null);
            image.setContent(myImg);

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Could not initialize file system to it's default state", ex);
            return;
        }

        try {
            pi.serve();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Could not run the server", ex);
        }
    }
}
