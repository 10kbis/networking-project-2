import java.util.Date;

public class File implements FSNode {
    private String name;
    private User owner;
    private Date modification_date;
    private byte[] data = {};

    public File(String name, User owner) {
        setName(name);
        setOwner(owner);
        setModificationDate(new Date());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public synchronized void setName(String name) {
        this.name = name;
    }

    @Override
    public User getOwner() {
        return owner;
    }

    @Override
    public Date getModificationDate() {
        return modification_date;
    }

    @Override
    public synchronized void setModificationDate(Date date) {
        modification_date = date;
    }

    private synchronized void setOwner(User owner) {
        this.owner = owner;
    }
/*
    public void clearContent() {

    }*/

    public synchronized void setContent(byte[] data) {
        this.data = data.clone();
        setModificationDate(new Date());
    }
/*
    public synchronized void appendContent(byte[] append_data) {
        byte[] new_data = new byte[data.length + append_data.length];
        System.arraycopy(data, 0, new_data, 0, data.length);
        System.arraycopy(append_data, 0, new_data, data.length, append_data.length);

        data = new_data;
    }*/

    public byte[] getContent() {
        return data.clone();
    }
}
