import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProtocolInterpreter {
    private static final Logger logger = Logger.getLogger(ProtocolInterpreter.class.getName());
    private static final String delimiter = "\r\n";
    private static final List<String> features = new ArrayList<>();
    private final int control_port;
    private final int data_port;
    private final ExecutorService thread_pool;
    private final HashMap<String, User> users = new HashMap<>();
    private final FileSystem fs;

    static {
        features.add("MDMT");
    }

    /**
     * Creates a server with a specified port
     * @param control_port Port to listen for commands
     * @param data_port Port to listen for data
     * @param max_threads Number of threads to serve the clients
     */
    public ProtocolInterpreter(int control_port, int data_port, int max_threads, FileSystem fs) {
        this.control_port = control_port;
        this.data_port = data_port;
        thread_pool = Executors.newFixedThreadPool(max_threads);
        this.fs = fs;
    }

    /**
     * Starts the server.
     * This method will loop indefinitely.
     * @throws IOException If the server could not be created
     */
    public void serve() throws IOException {
        ServerSocket serv_socket = new ServerSocket(control_port);

        logger.info("Server running (command port " + control_port +", data port " + data_port + ")...");
        while (true) {
            try {
                Socket client = serv_socket.accept();
                client.setSoTimeout(10 * 60 * 1000); // 10 minutes

                logger.info("New client: " + client.getRemoteSocketAddress().toString());


                thread_pool.execute(() -> handleClient(client));
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }

    public void addUser(User user) {
        users.put(user.getName(), user);
    }

    private void handleClient(Socket client) {
        Session session = new Session();
        session.host = client;

        try {
            InputStreamReader isr = new InputStreamReader(client.getInputStream());
            OutputStreamWriter osw = new OutputStreamWriter(client.getOutputStream());

            session.in_stream = new BufferedReader(isr);
            session.out_stream = new BufferedWriter(osw);

            logger.info("Sending server is ready message");
            session.out_stream.write(FTPReply.SERVICE_READY + delimiter);
            session.out_stream.flush();

            while (true) {
                String request = session.in_stream.readLine();
                if (request == null) {
                    break;
                }

                FTPReply response;

                try {
                    response = onRequest(request, session);
                } catch (Exception ex) {
                    logger.severe("An internal error occured while processing request: " + request);
                    response = FTPReply.INTERNAL_ERROR;
                }

                if (response != null) {
                    logger.info("Response: " + response);
                    session.out_stream.write(response + delimiter);
                    session.out_stream.flush();
                }
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "IO error", ex);
        }

        try {
            client.close();
            logger.info("Client disconnected");
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Could not close the connection with the client", ex);
        }
    }

    private FTPReply onRequest(String request, Session session) throws IOException {
        logger.info("Request: " + request);
        String[] args = request.split(" ");
        String verb = args[0].toLowerCase();

        String previous_cmd = session.previous_cmd;
        session.previous_cmd = verb;

        switch (verb) {
            case "cdup": {
                if (args.length != 1) {
                    logger.warning("Request CDUP malformed");
                    return FTPReply.ERROR_PARAM;
                }

                if (!session.logged_in) {
                    logger.warning("Trying to CDUP while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                session.working_dir = session.working_dir.parent();
                return FTPReply.CMD_OK;
            }
            case "cwd": {
                if (args.length < 2) {
                    logger.warning("Request CDW malformed");
                    return FTPReply.ERROR_PARAM;
                }

                if (!session.logged_in) {
                    logger.warning("Trying to CDW while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                Path dir = new Path(request.substring(4));
                Path new_wd = session.working_dir.concat(dir);

                try {
                    Directory wd = fs.getDirectory(new_wd, session.user);
                    if (wd != null) {
                        if (!User.isEqualOrMorePrivileged(session.user, wd.getOwner())) {
                            throw new NotEnoughPrivileges();
                        }
                        session.working_dir = new_wd;
                        logger.info("New working directory is " + new_wd);
                        return FTPReply.ACTION_TAKEN;
                    } else {
                        return FTPReply.NOT_FOUND;
                    }
                } catch (NotEnoughPrivileges notEnoughPrivileges) {
                    return FTPReply.NOT_ENOUGH_PRIVILEGES;
                } catch (NotADirectory notADirectory) {
                    return FTPReply.NOT_FOUND;
                }
            }
            case "dele": {
                if (args.length < 2) {
                    logger.warning("Request DELE malformed");
                    return FTPReply.ERROR_PARAM;
                }

                if (!session.logged_in) {
                    logger.warning("Trying to DELE while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                Path requested = new Path(request.substring(5));
                requested = session.working_dir.concat(requested);

                try {
                    fs.removeFile(requested, session.user);
                    return FTPReply.ACTION_TAKEN;
                } catch (NotADirectory notADirectory) {
                    return FTPReply.NOT_FOUND;
                } catch (NotEnoughPrivileges notEnoughPrivileges) {
                    return FTPReply.NOT_ENOUGH_PRIVILEGES;
                }
            }
            case "feat": {
                if (args.length != 1) {
                    logger.warning("Request FEAT malformed");
                    return FTPReply.ERROR_PARAM;
                }

                return FTPReply.makeFeatureReply(features);
            }
            case "list": {
                if (!session.logged_in) {
                    logger.warning("Trying to LIST while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                Path requested_path = session.working_dir;

                if (args.length > 1) {
                    requested_path = requested_path.concat(new Path(request.substring(5)));
                }

                logger.info("Listing directory " + requested_path);

                try {
                    List<FSNode> content;
                    Directory dir = fs.getDirectory(requested_path, session.user);
                    if (dir == null) {
                        File file = fs.getFile(requested_path, session.user);
                        if (file == null) {
                            return FTPReply.NOT_FOUND;
                        }
                        content = new ArrayList<>();
                        content.add(file);
                    } else {
                        content = dir.listContent(session.user);
                    }
                    if (session.transferProcess == null) {
                        throw new DataConnectionNotOpened();
                    }
                    session.out_stream.write(FTPReply.START_TRANSFER + delimiter);
                    session.out_stream.flush();
                    session.transferProcess.sendDirectoryListing(content);

                    return FTPReply.CLOSE_DATA_CONNECTION;
                } catch (NotEnoughPrivileges notEnoughPrivileges) {
                    String name = session.user == null ? "anonymous" : session.user.getName();
                    logger.info("Access to " + requested_path + " is not allowed for user " + name);
                    return FTPReply.NOT_ENOUGH_PRIVILEGES;
                } catch (NotADirectory notADirectory) {
                    logger.info(requested_path + " is not a directory");
                    return FTPReply.NOT_FOUND;
                } catch (DataConnectionNotOpened dataConnectionNotOpened) {
                    logger.warning("Not data connection opened, data can't be sent");
                    return FTPReply.CANT_OPEN_DATA_CONNECTION;
                } catch (TransferAborted transferAborted) {
                    logger.warning("Error while transferring data, transfer aborted");
                    return FTPReply.TRANSFER_ABORTED;
                }
            }
            case "mdtm": {
                if (!session.logged_in) {
                    logger.warning("Trying to MDTM while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                if (args.length < 2) {
                    logger.warning("Received malformed MDTM request");
                    return FTPReply.ERROR_PARAM;
                }

                try {
                    Path requested = new Path(request.substring(5));
                    requested = session.working_dir.concat(requested);
                    FSNode node = fs.getNode(requested, session.user);

                    if (node == null) {
                        return FTPReply.NOT_FOUND;
                    } else {
                        return FTPReply.makeModificationTimeReply(node.getModificationDate());
                    }
                } catch (NotEnoughPrivileges notEnoughPrivileges) {
                    return FTPReply.NOT_ENOUGH_PRIVILEGES;
                } catch (NotADirectory notADirectory) {
                    return FTPReply.NOT_FOUND;
                }
            }
            case "pass": {
                String try_user = session.try_connect_as_user;
                session.try_connect_as_user = null;

                if (args.length < 2) {
                    logger.warning("Received malformed PASS request");
                    return FTPReply.ERROR_PARAM;
                }

                if (try_user == null || !"user".equals(previous_cmd)) {
                    logger.warning("Received PASS request without a USER request before");
                    return FTPReply.BAD_SEQUENCE;
                }

                User user = users.get(try_user);

                if (user == null) {
                    logger.info("User '" + try_user + "' is not a valid user");
                    return FTPReply.NOT_LOGGED_IN;
                }

                String password = request.substring(5);

                if (user.isPasswordValid(password)) {
                    logger.info("User '" + try_user + "' connected");
                    session.logged_in = true;
                    session.user = user;
                    return FTPReply.LOGGED_IN;
                } else {
                    logger.info("User '" + try_user + "' entered a bad password");
                    return FTPReply.NOT_LOGGED_IN;
                }
            }
            case "pasv": {
                if (!session.logged_in) {
                    logger.warning("Trying to PASV while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                if (args.length != 1) {
                    logger.warning("Request PASV malformed");
                    return FTPReply.ERROR_PARAM;
                }

                Random rand = new Random();
                // Worst way to choose a port but whatever...
                int port = 57000 + rand.nextInt(1000);

                try {
                    session.transferProcess = DataTransferProcess.newPassiveConnection(port, session.type);
                    logger.info("Listening for passive connection on " + session.host.getLocalAddress() + ":" + port);

                    FTPReply reply = FTPReply.makePassiveReply(session.host.getLocalAddress(), port);
                    logger.info("Sending PASV reply to the client: " + reply);
                    session.out_stream.write(reply + delimiter);
                    session.out_stream.flush();

                    logger.info("Waiting from host to connect on data connection...");
                    session.transferProcess.connect();
                    logger.info("Host connected");
                } catch (DataConnectionNotOpened ex) {
                    logger.warning("Could not connect to the host for data transfer");
                }

                return null;
            }
            case "port": {
                if (!session.logged_in) {
                    logger.warning("Trying to PORT while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                if (args.length != 2) {
                    logger.warning("Request PORT malformed");
                    return FTPReply.ERROR_PARAM;
                }

                String[] components = args[1].split(",");

                if (components.length != 6) {
                    logger.warning("Request PORT malformed");
                    return FTPReply.ERROR_PARAM;
                }

                try {
                    String host = components[0] + "." + components[1] + "." + components[2] + "." + components[3];
                    // ignore port in parameter and always use the same ?
                    InetSocketAddress address = new InetSocketAddress(host, data_port);

                    logger.info("Connecting to host in active mode to " + address + "...");
                    session.transferProcess = DataTransferProcess.newActiveConnection(address, session.type);
                    session.transferProcess.connect();
                    logger.info("Connected to host");

                    return FTPReply.CMD_OK;
                } catch (RuntimeException | DataConnectionNotOpened ex) {
                    return FTPReply.ERROR_PARAM;
                }
            }
            case "pwd": {
                if (!session.logged_in) {
                    logger.warning("Trying to PWD while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                if (args.length != 1) {
                    logger.warning("Request PWD malformed");
                    return FTPReply.ERROR_PARAM;
                }

                return FTPReply.makePathReply(session.working_dir);
            }
            case "rnfr": {
                if (!session.logged_in) {
                    logger.warning("Trying to RNTO while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                if (args.length < 2) {
                    logger.warning("Request RNTO malformed");
                    return FTPReply.ERROR_PARAM;
                }

                session.rename_from = new Path(request.substring(5));
                try {
                    if (fs.getFile(session.rename_from, session.user) != null) {
                        return FTPReply.MORE_INFO_NEEDED;
                    } else {
                        return FTPReply.NOT_FOUND;
                    }
                } catch (NotEnoughPrivileges notEnoughPrivileges) {
                    logger.info("Require privileges to rename " + session.rename_from);
                    return FTPReply.NOT_ENOUGH_PRIVILEGES;
                } catch (NotADirectory notADirectory) {
                    logger.info(session.rename_from + "not found");
                    return FTPReply.NOT_FOUND;
                }
            }
            case "rnto": {
                if (!session.logged_in) {
                    logger.warning("Trying to RNTO while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                if (args.length < 2) {
                    logger.warning("Request RNTO malformed");
                    return FTPReply.ERROR_PARAM;
                }

                if (session.rename_from == null || !"rnfr".equals(previous_cmd)) {
                    logger.warning("Received RNTO request without a RNFR request before");
                    return FTPReply.BAD_SEQUENCE;
                }

                Path old_name = session.rename_from;
                Path new_name = new Path(request.substring(5));
                try {
                    if (fs.rename(old_name, new_name, session.user)) {
                        logger.info("Renamed " + old_name + " to " + new_name);
                        return FTPReply.ACTION_TAKEN;
                    } else {
                        logger.info("Could not rename " + old_name + " to " + new_name);
                        return FTPReply.FILENAME_NOT_ALLOWED;
                    }
                } catch (NotADirectory notADirectory) {
                    logger.info(old_name + " or " + new_name + " not found");
                    return FTPReply.FILENAME_NOT_ALLOWED;
                } catch (NotEnoughPrivileges notEnoughPrivileges) {
                    logger.info("Require privileges to rename " + old_name + " to " + new_name);
                    return FTPReply.FILENAME_NOT_ALLOWED;
                }
            }
            case "retr": {
                if (!session.logged_in) {
                    logger.warning("Trying to RETR while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                if (args.length < 2) {
                    logger.warning("Request RETR malformed");
                    return FTPReply.ERROR_PARAM;
                }

                Path requested = new Path(request.substring(5));
                requested = session.working_dir.concat(requested);

                try {
                    File file = fs.getFile(requested, session.user);
                    if (file == null) {
                        return FTPReply.NOT_FOUND;
                    }

                    if (session.transferProcess == null) {
                        throw new DataConnectionNotOpened();
                    }
                    session.out_stream.write(FTPReply.START_TRANSFER + delimiter);
                    session.out_stream.flush();
                    session.transferProcess.uploadToHost(file);

                    return FTPReply.CLOSE_DATA_CONNECTION;
                } catch (NotEnoughPrivileges notEnoughPrivileges) {
                    String name = session.user == null ? "anonymous" : session.user.getName();
                    logger.info("Access to " + requested + " is not allowed for user " + name);
                    return FTPReply.NOT_ENOUGH_PRIVILEGES;
                } catch (NotADirectory notADirectory) {
                    logger.info(notADirectory.getMessage());
                    return FTPReply.NOT_FOUND;
                } catch (DataConnectionNotOpened dataConnectionNotOpened) {
                    logger.warning("Not data connection opened, data can't be sent");
                    return FTPReply.CANT_OPEN_DATA_CONNECTION;
                } catch (TransferAborted transferAborted) {
                    logger.warning("Error while transferring data, transfer aborted");
                    return FTPReply.TRANSFER_ABORTED;
                }
            }
            case "stor": {
                if (!session.logged_in) {
                    logger.warning("Trying to STOR while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                if (args.length < 2) {
                    logger.warning("Request STOR malformed");
                    return FTPReply.ERROR_PARAM;
                }

                Path requested = new Path(request.substring(5));
                requested = session.working_dir.concat(requested);

                try {
                    File file;
                    try {
                        file = fs.makeFile(requested, session.user);
                    } catch (NodeAlreadyExists nodeAlreadyExists) {
                        file = fs.getFile(requested, session.user);
                        if (file == null) {
                            return FTPReply.NOT_FOUND;
                        }
                    }

                    if (session.transferProcess == null) {
                        throw new DataConnectionNotOpened();
                    }
                    session.out_stream.write(FTPReply.START_TRANSFER + delimiter);
                    session.out_stream.flush();
                    session.transferProcess.downloadFromHost(file);

                    return FTPReply.CLOSE_DATA_CONNECTION;
                } catch (NotEnoughPrivileges notEnoughPrivileges) {
                    String name = session.user == null ? "anonymous" : session.user.getName();
                    logger.info("Access to " + requested + " is not allowed for user " + name);
                    return FTPReply.NOT_ENOUGH_PRIVILEGES;
                } catch (NotADirectory notADirectory) {
                    logger.info(notADirectory.getMessage());
                    return FTPReply.NOT_FOUND;
                } catch (DataConnectionNotOpened dataConnectionNotOpened) {
                    logger.warning("Not data connection opened, data can't be received");
                    return FTPReply.CANT_OPEN_DATA_CONNECTION;
                } catch (TransferAborted transferAborted) {
                    logger.warning("Error while transferring data, transfer aborted");
                    return FTPReply.TRANSFER_ABORTED;
                }
            }
            case "type": {
                if (args.length != 2) {
                    logger.warning("Request TYPE malformed");
                    return FTPReply.ERROR_PARAM;
                }

                if (!session.logged_in) {
                    logger.warning("Trying to TYPE while not logged in");
                    return FTPReply.NOT_LOGGED_IN;
                }

                String str_type = args[1];

                switch (str_type.toUpperCase()) {
                    case "I": {
                        logger.info("Transfer type is now BINARY");
                        session.type = FTPType.BINARY;
                        return FTPReply.CMD_OK;
                    }
                    case "A": {
                        logger.info("Transfer type is now ASCII");
                        session.type = FTPType.ASCII;
                        return FTPReply.CMD_OK;
                    }
                    default: {
                        logger.info("Parameter '" + str_type + "' for TYPE is not implemented");
                        return FTPReply.NOT_IMPL_PARAM;
                    }
                }
            }
            case "user": {
                if (args.length != 2) {
                    logger.warning("Received malformed USER request");
                    return FTPReply.ERROR_PARAM;
                }

                String user = args[1];

                if ("anonymous".equals(user)) {
                    logger.info("User 'anonymous' connected");
                    session.logged_in = true;
                    session.user = null;
                    return FTPReply.LOGGED_IN;
                } else if (users.containsKey(user)) {
                    logger.info("User '" + user + "' trying to connect");
                    session.try_connect_as_user = user;
                    return FTPReply.NEED_PASSWORD;
                } else {
                    logger.info("User '" + user + "' is not a valid user");
                    return FTPReply.NOT_LOGGED_IN;
                }
            }
            case "syst": {
                if (args.length != 1) {
                    logger.warning("Received malformed SYST request");
                    return FTPReply.ERROR_PARAM;
                }

                // wtf is this command, FTP spec is a bit weird (and outdated)
                // See https://cr.yp.to/ftp/syst.html for more info
                return FTPReply.makeSystemNameReply("UNIX Type: L8");
            }
            default: {
                logger.warning("Unknown request '" + verb.toUpperCase() + "' received");
                return FTPReply.UNKNOWN_CMD;
            }
        }
    }
}
