import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class FileSystemTest {

    @Test
    void makeDirectoryDuplicate() throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        FileSystem fs = new FileSystem();
        User mt = new User("mt", "123");

        fs.makeDirectory("/home", null);
        fs.makeDirectory("/home/mt", mt);

        assertThrows(NodeAlreadyExists.class, () -> {
            fs.makeDirectory("/home", null);
        });

        assertThrows(NodeAlreadyExists.class, () -> {
            fs.makeDirectory("/home", mt);
        });

        assertThrows(NodeAlreadyExists.class, () -> {
            fs.makeDirectory("/home/mt", mt);
        });
    }

    @Test
    void makeDirectoryNoPrivilege() throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        FileSystem fs = new FileSystem();
        User mt = new User("mt", "123");

        fs.makeDirectory("/mt", mt);

        assertThrows(NotEnoughPrivileges.class, () -> {
            fs.makeDirectory("/mt/test", null);
        });
    }

    @Test
    void makeDirectoryRoot() throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        FileSystem fs = new FileSystem();

        assertThrows(NodeAlreadyExists.class, () -> {
            fs.makeDirectory("/", null);
        });
    }

    @Test
    void makeDirectoryAlreadyExists() throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        FileSystem fs = new FileSystem();
        User mt = new User("mt", "123");

        fs.makeDirectory("/mt", null);

        assertThrows(NodeAlreadyExists.class, () -> {
            fs.makeDirectory("/mt", null);
        });

        assertThrows(NodeAlreadyExists.class, () -> {
            fs.makeDirectory("/mt", mt);
        });
    }


    @Test
    void getFile() {
    }
}