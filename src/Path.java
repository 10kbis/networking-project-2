import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Path {
    private boolean absolute;
    private List<String> components;

    public Path() {
        components = new ArrayList<>();
    }

    public Path(String path) {
        setAbsolute(path.startsWith("/"));

        if (isAbsolute()) {
            path = path.substring(1);
        }

        if (path.length() > 0) {
            components = Arrays.asList(path.split("/"));
        } else {
            components = new ArrayList<>();
        }

    }

    private Path(List<String> components) {
        this.components = components;
    }

    public Path append(String name) {
        Path new_path = clone();
        new_path.components.add(name);

        return new_path;
    }

    public Path concat(Path other) {
        if (other.isAbsolute()) {
            return other;
        }

        Path result = clone();
        result.components.addAll(other.components);

        return result;
    }

    public Path parent() {
        Path new_path = clone();

        if (!new_path.components.isEmpty()) {
            new_path.components.remove(new_path.components.size() - 1);
        }

        return new_path;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) return false;

        return obj.toString().equals(toString());
    }

    @Override
    public String toString() {
        return "/" + String.join("/", components);
    }

    @Override
    public Path clone() {
        List<String> new_components = new ArrayList<>();

        if (components != null) {
            new_components.addAll(components);
        }

        return new Path(new_components);
    }

    public List<String> getComponents() {
        return components;
    }

    public String getLastComponent() {
        if (components.isEmpty()) {
            return null;
        }

        return components.get(components.size() - 1);
    }

    public boolean isAbsolute() {
        return absolute;
    }

    private void setAbsolute(boolean absolute) {
        this.absolute = absolute;
    }
}
