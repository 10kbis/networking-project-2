import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Directory implements FSNode {
    private String name;
    private User owner;
    private Date modification_date;
    private List<FSNode> nodes = new ArrayList<>();

    public Directory(String name, User owner) {
        setName(name);
        setOwner(owner);
        setModificationDate(new Date());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public synchronized void setName(String name) {
        this.name = name;
    }

    @Override
    public User getOwner() {
        return owner;
    }

    private synchronized void setOwner(User owner) {
        this.owner = owner;
    }

    @Override
    public Date getModificationDate() {
        return modification_date;
    }

    @Override
    public synchronized void setModificationDate(Date date) {
        modification_date = date;
    }

    public synchronized FSNode getNode(String name, User as_user) throws NotEnoughPrivileges {
        if (!User.isEqualOrMorePrivileged(as_user, getOwner())) {
            throw new NotEnoughPrivileges();
        }

        for (FSNode node: nodes) {
            if (node.getName().equals(name)) {
                return node;
            }
        }
        return null;
    }

    private synchronized boolean containsNode(String name) {
        for (FSNode node: nodes) {
            if (node.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }

    public synchronized void addNode(FSNode node) throws NodeAlreadyExists, NotEnoughPrivileges {
        if (!User.isEqualOrMorePrivileged(node.getOwner(), getOwner())) {
            throw new NotEnoughPrivileges();
        }

        if (containsNode(node.getName())) {
            throw new NodeAlreadyExists();
        }

        nodes.add(node);
        setModificationDate(new Date());
    }

    public synchronized boolean removeNode(String name, User user) throws NotEnoughPrivileges {
        if (!User.isEqualOrMorePrivileged(user, getOwner())) {
            throw new NotEnoughPrivileges();
        }

        FSNode to_delete = null;

        for (FSNode node: nodes) {
            if (node.getName().equals(name) && User.isEqualOrMorePrivileged(user, node.getOwner())) {
                to_delete = node;
                break;
            }
        }

        if (to_delete != null) {
            nodes.remove(to_delete);
            return true;
        }

        return false;
    }

    public synchronized List<FSNode> listContent(User as_user) throws NotEnoughPrivileges {
        if (!User.isEqualOrMorePrivileged(as_user, getOwner())) {
            throw new NotEnoughPrivileges();
        }

        return  nodes;
    }

    public synchronized int size() {
        return nodes.size();
    }
}
