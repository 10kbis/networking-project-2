import java.util.logging.Logger;

public class FileSystem {
    Logger logger = Logger.getLogger("FileSystem");

    private Directory root = new Directory("/", null);

    public FSNode getNode(Path path, User as_user) throws NotEnoughPrivileges, NotADirectory {
        Directory current_dir;
        FSNode current_node = root;
        Path so_far = new Path();

        for (String name: path.getComponents()) {
            if (current_node instanceof Directory) {
                current_dir = (Directory) current_node;
            } else {
                throw new NotADirectory(so_far);
            }

            so_far.append(name);
            current_node = current_dir.getNode(name, as_user);
        }

        return current_node;
    }

    public Directory getDirectory(Path path, User as_user) throws NotEnoughPrivileges, NotADirectory {
        logger.info("Get directory '" + path + "' as user '" + as_user + "'");

        FSNode n = getNode(path, as_user);
        if (n instanceof Directory) {
            return (Directory) n;
        }
        return null;
    }

    public Directory getDirectory(String path, User as_user) throws NotEnoughPrivileges, NotADirectory {
        return getDirectory(new Path(path), as_user);
    }

    public File getFile(Path path, User as_user) throws NotEnoughPrivileges, NotADirectory {
        logger.info("Get file '" + path + "' as user '" + as_user + "'");

        FSNode n = getNode(path, as_user);
        if (n instanceof File) {
            return (File) n;
        }
        return null;
    }

    public File getFile(String path, User as_user) throws NotEnoughPrivileges, NotADirectory {
        return getFile(new Path(path), as_user);
    }

    private synchronized void addNode(Path path, FSNode node) throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        logger.info("Add node " + node + " to " + path);

        Path parent = path.parent();
        if (parent.equals(path)) {
            throw new NodeAlreadyExists();
        }

        FSNode parent_node = getNode(parent, node.getOwner());

        if (!(parent_node instanceof Directory)) {
            throw new NotADirectory(parent);
        }

        Directory parent_dir = (Directory) parent_node;

        parent_dir.addNode(node);
    }

    public synchronized Directory makeDirectory(Path path, User owner) throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        logger.info("Make directory " + path + " as user " + owner);

        String name = path.getLastComponent();
        Directory new_dir = new Directory(name, owner);
        addNode(path, new_dir);
        return new_dir;
    }

    public Directory makeDirectory(String path, User owner) throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        return makeDirectory(new Path(path), owner);
    }

    public synchronized File makeFile(Path path, User owner) throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        logger.info("Make file " + path + " as user " + owner);

        String name = path.getLastComponent();
        File new_file = new File(name, owner);
        addNode(path, new_file);
        return new_file;
    }

    public File makeFile(String path, User owner) throws NotEnoughPrivileges, NodeAlreadyExists, NotADirectory {
        return makeFile(new Path(path), owner);
    }

    public synchronized boolean removeFile(Path path, User user) throws NotADirectory, NotEnoughPrivileges {
        logger.info("Remove file " + path + " as user " + user);

        Directory parent = getDirectory(path.parent(), user);
        if (parent == null) {
            return false;
        }

        return parent.removeNode(path.getLastComponent(), user);
    }

    public synchronized boolean rename(Path old_path, Path new_path, User user) throws NotADirectory, NotEnoughPrivileges {
        logger.info("Rename " + old_path + " to " + new_path + " as user " + user);

        FSNode node = getNode(old_path, user);
        if (node == null) {
            logger.info("Could not find " + old_path + " as user " + user);
            return false;
        }

        removeFile(old_path, user);

        String old_name = node.getName();
        node.setName(new_path.getLastComponent());

        try {
            Directory parent = getDirectory(new_path.parent(), user);
            if (parent == null) {
                throw new NotADirectory(new_path.parent());
            }
            parent.addNode(node);
            return true;
        } catch (NotEnoughPrivileges | NotADirectory | NodeAlreadyExists notEnoughPrivileges) {
            logger.info("Could not add node " + node + " to " + new_path + " as user " + user);
            try {
                node.setName(old_name);
                Directory parent = getDirectory(old_path.parent(), user);
                parent.addNode(node);
                return false;
            } catch (NodeAlreadyExists nodeAlreadyExists) {
                logger.severe("Could not reattach node " + node + " to " + old_path + " as user " + user);
                // can't happen as the node was already attached at the start of the function
                return false;
            }
        }
    }
}
