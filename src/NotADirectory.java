public class NotADirectory extends Exception {

    public NotADirectory(Path path) {
        super("'" + path + "' is not a directory");
    }
}
