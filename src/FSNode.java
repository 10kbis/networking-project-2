import java.util.Date;

public interface FSNode {
    String getName();
    void setName(String name);

    User getOwner();

    Date getModificationDate();
    void setModificationDate(Date date);

}
