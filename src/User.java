import java.util.Objects;

public class User {
    private String name;
    private String password;

    public User(String name, String password) {
        setName(name);
        setPassword(password);
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isPasswordValid(String password) {
        if (this.password == null) {
            return password == null;
        }
        return this.password.equals(password);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return name.equals(user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public static boolean isEqualOrMorePrivileged(User u1, User u2) {
        if (u1 == null && u2 == null) return true;
        if (u1 == null && u2 != null) return false;
        if (u1 != null && u2 == null) return true;
        return u1.equals(u2);
    }
}
